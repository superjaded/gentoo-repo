# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

QOGIRICONPATH="/usr/share/icons"

DESCRIPTION="Qogir icon theme"
HOMEPAGE="https://github.com/vinceliuice/Qogir-icon-theme"


if [[ "${PV}" == "99999999" ]] ; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/vinceliuice/Qogir-icon-theme.git"
else
	SRC_URI="https://github.com/vinceliuice/Qogir-icon-theme/archive/${QOGIRRELEASEDATE}.tar.gz -> ${P}.tar.gz"
	KEYWORDS="amd64 x86"
	S="${WORKDIR}/Qogir-icon-theme-${QOGIRRELEASEDATE}"
fi

LICENSE=""
SLOT="0"
IUSE=""


DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

src_install()
{
	dodir "${QOGIRICONPATH}"
	${S}/install.sh -d "${D}/${QOGIRICONPATH}"
}
