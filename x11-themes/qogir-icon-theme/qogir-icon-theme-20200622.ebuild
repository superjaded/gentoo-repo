# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

QOGIRRELEASEDATE="2020-06-22"
QOGIRICONPATH="/usr/share/icons"

DESCRIPTION="Qogir icon theme"
HOMEPAGE="https://github.com/vinceliuice/Qogir-icon-theme"
SRC_URI="https://github.com/vinceliuice/Qogir-icon-theme/archive/${QOGIRRELEASEDATE}.tar.gz -> ${P}.tar.gz"

LICENSE=""
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

S="${WORKDIR}/Qogir-icon-theme-${QOGIRRELEASEDATE}"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

src_install()
{
	dodir "${QOGIRICONPATH}"
	${S}/install.sh -d "${D}/${QOGIRICONPATH}"
}
