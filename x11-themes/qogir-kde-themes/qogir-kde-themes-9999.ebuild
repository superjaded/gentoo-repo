# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2


EAPI=7

inherit git-r3

DESCRIPTION="KDE specific and distro agnostic themes for Qogir"
HOMEPAGE="https://github.com/vinceliuice/Qogir-kde"
EGIT_REPO_URI="https://github.com/vinceliuice/Qogir-kde.git"
SHARE_ROOT="/usr/share"

LICENSE=""
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

src_install()
{
	dodir ${SHARE_ROOT}/aurorae/themes
	cp -R ${S}/aurorae/themes/* ${D}${SHARE_ROOT}/aurorae/themes
	dodir ${SHARE_ROOT}/color-schemes
	cp -R ${S}/color-schemes/* ${D}${SHARE_ROOT}/color-schemes
	dodir ${SHARE_ROOT}/Kvantum
	cp -R ${S}/Kvantum/Qogir ${D}${SHARE_ROOT}/Kvantum
	cp -R ${S}/Kvantum/Qogir-dark ${D}${SHARE_ROOT}/Kvantum
	cp -R ${S}/Kvantum/Qogir-dark-solid ${D}${SHARE_ROOT}/Kvantum
	cp -R ${S}/Kvantum/Qogir-light ${D}${SHARE_ROOT}/Kvantum
	cp -R ${S}/Kvantum/Qogir-light-solid ${D}${SHARE_ROOT}/Kvantum
	cp -R ${S}/Kvantum/Qogir-solid ${D}${SHARE_ROOT}/Kvantum
	dodir ${SHARE_ROOT}/plasma/desktoptheme
	cp -R ${S}/plasma/desktoptheme/Qogir ${D}${SHARE_ROOT}/plasma/desktoptheme/
	cp -R ${S}/plasma/desktoptheme/icons ${D}${SHARE_ROOT}/plasma/desktoptheme/Qogir/icons
	cp -R ${S}/plasma/desktoptheme/Qogir-dark ${D}${SHARE_ROOT}/plasma/desktoptheme/
	cp -R ${S}/plasma/desktoptheme/icons-dark ${D}${SHARE_ROOT}/plasma/desktoptheme/Qogir-dark/icons
	dodir ${SHARE_ROOT}/plasma/look-and-feel
	cp -R ${S}/plasma/look-and-feel/com.github.vinceliuice.Qogir ${D}${SHARE_ROOT}/plasma/look-and-feel/
	cp -R ${S}/plasma/look-and-feel/com.github.vinceliuice.Qogir-light ${D}${SHARE_ROOT}/plasma/look-and-feel/
	cp -R ${S}/plasma/look-and-feel/com.github.vinceliuice.Qogir-dark ${D}${SHARE_ROOT}/plasma/look-and-feel/
	dodir ${SHARE_ROOT}/sddm/themes
	cp -R ${S}/sddm/Qogir ${D}${SHARE_ROOT}/sddm/themes/
}
