# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2


EAPI=7

inherit git-r3

DESCRIPTION="GTK theme based on Qogir made to match KDE theme"
HOMEPAGE="https://github.com/freefreeno/Kogir-Dark,https://github.com/vinceliuice/Qogir-kde"
EGIT_REPO_URI="https://github.com/freefreeno/Kogir-Dark.git"
SHARE_ROOT="/usr/share"

LICENSE=""
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

src_install()
{
    # /var/tmp/portage/x11-themes/kogir-dark-9999/work/kogir-dark-9999/GTK/Kogir-dark
    dodir "${SHARE_ROOT}/themes"
	cp -R "${S}/GTK/Kogir-dark" "${D}/${SHARE_ROOT}/themes"
}
