# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

QOGIRRELEASEDATE="2020-02-26"
QOGIRTHEMEPATH="/usr/share/themes"

DESCRIPTION="Qogir themes for various GTK+ apps"
HOMEPAGE="https://github.com/vinceliuice/Qogir-theme"
if [[ ${PV} == "99999999" ]];then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/vinceliuice/Qogir-theme.git"
else
	SRC_URI="https://github.com/vinceliuice/Qogir-theme/archive/${QOGIRRELEASEDATE}.tar.gz -> ${P}.tar.gz"
	S="${WORKDIR}/Qogir-theme-${QOGIRRELEASEDATE}"
fi
# https://github.com/vinceliuice/Qogir-theme/archive/2020-02-26.tar.gz

LICENSE=""
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""


DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

src_install()
{
	dodir "${QOGIRTHEMEPATH}"
	${S}/install.sh -d "${D}/${QOGIRTHEMEPATH}" -l gentoo
}
