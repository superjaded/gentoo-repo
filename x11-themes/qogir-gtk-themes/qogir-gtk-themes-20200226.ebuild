# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

QOGIRRELEASEDATE="2020-02-26"
QOGIRTHEMEPATH="/usr/share/themes"

DESCRIPTION="Qogir themes for various GTK+ apps"
HOMEPAGE="https://github.com/vinceliuice/Qogir-theme"
SRC_URI="https://github.com/vinceliuice/Qogir-theme/archive/${QOGIRRELEASEDATE}.tar.gz -> ${P}.tar.gz"
# https://github.com/vinceliuice/Qogir-theme/archive/2020-02-26.tar.gz

LICENSE=""
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

S="${WORKDIR}/Qogir-theme-${QOGIRRELEASEDATE}"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

src_install()
{
	dodir "${QOGIRTHEMEPATH}"
	${S}/install.sh -d "${D}/${QOGIRTHEMEPATH}" -t standard
}
