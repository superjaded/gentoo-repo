# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2


EAPI=7

inherit git-r3

DESCRIPTION="distro agnostic wallpapers for Qogir"
HOMEPAGE="https://github.com/vinceliuice/Qogir-kde"
EGIT_REPO_URI="https://github.com/vinceliuice/Qogir-kde.git"
SHARE_ROOT="/usr/share"

LICENSE=""
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

src_install()
{
	dodir ${SHARE_ROOT}/wallpapers
	cp -R ${S}/wallpaper/Qogir ${D}${SHARE_ROOT}/wallpapers/
	cp -R ${S}/wallpaper/Qogir-dark ${D}${SHARE_ROOT}/wallpapers/
}
